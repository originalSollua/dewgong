import discord
import random
import msgHand
import sys
import queue
import threading
import time
from discord.ext import commands


def thread_task_runner(taskArray):
    # task running thread
    # runs continuously in the background and processes 
    # draw task objects from the taskArray
    # taskArray is an instance in the Dewgong Class
    print("Task Processor Initialized")
    while(True):
        if len(taskArray) > 0:
            # tasks to do
            curent = taskArray.pop()
            if curent.taskType == "init":
                print("init task purged")
            elif curent.taskType == "new":
                print(curent.data)
            else:
                print("Unrecognized task type")
        else:
            print("No tasks. sleeping 30 sec. ",threading.get_ident())
            time.sleep(30)


class task:
    # task characteristis:
    # a dewgong task is something we don't want to waste processer cycles in the bot doing. 
    # an example of this would be converting a message into boomercoin or saving logs or internal data
    # different tasks might be defined to have different internal data
    def __init__(self, taskT, dat):
        self.taskType = taskT
        self.data = dat


class Dewgong(discord.Client):
    taskList = []
    taskList.append(task("init", "Init task - do nothing"))
    exitCmd = "Safe shutdown requested."
    # add in a class that can handle message sending
    # make it a proc or something that can be run as a thread
    # the thread exists to look at a messaging queue. if messages need to be sent
    # add them to the queue. the thread will handle pulling from it and then sending
    async def on_ready(self):
        runner = threading.Thread(target=thread_task_runner, args=(self.taskList,))
        runner.start()
        print('Dewgong is active', self.user)

    async def on_message(self, message):
        self.taskList.append(task("new", "new message, do nothing"))
        if message.author == self.user:
            return

        if message.content == "Dewgong?":
            await message.channel.send(content="I lived, Bitch.")

        msgText = msgHand.processMessage(message, self.user)

        if not msgText == "":
            msgText = message.author.mention +" " +msgText
            await message.channel.send(content=msgText)
            if self.exitCmd in msgText:
                sys.exit(0)


tokfile = open("../data/token", "r")
token = tokfile.readline()
print(token)
tokfile.close()
client = Dewgong()
client.run(token.strip())
