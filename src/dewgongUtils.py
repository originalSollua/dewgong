# dewgongUtils
# a collection of code segments that can be used to make other modules simpler
import random

# boomerHash
# accept a string of text and perform opperations to it to produce a numberic value
# this value will be returned to be banked by Dewgong for access to ther Dewgong functions
# algorithms:
# 1  pad the mesage out to 256 chars by looping the text or truncating the text to 256
# 2  shift the 256 char array left by 7
# 3  XOR the 256 msg by the shifted array 8 bits at a time, and interperet the result as a fixed 8
# 4  reassign the shifter as the rightmost 32 bits of the curent shifter
# 5  squish all results from 3 into one number
# 6  XOR 5 by 4
# 7  perform a prime factorization on 6, count the number of layers in the actor tree.
# *  apply a 1.2x multiplier if the text mentions Dewgong at all
# def boomerHash(msgstr):

# Roll Max
# accept a string of format ndm and returns the maximum value of n m sided dice.
# also accepts n, for the case of @dewgong roll 20
def rollMax(diceStr):
    nums = diceStr.split('d')
    retnum = 0
    # nums[0] is the number of times to roll
    # nums[1] is the size to roll
    # nums[0] * nums[1] is our boi
    # relying on the regex to catch our parsing here
    try: 
        retnum = int(nums[0]) * int(nums[1])
    except ValueError:
        # one of the inputs was not an integer
        retnum = -1
    except IndexError:
        # unable to get two numbers
        # case where we said roll 20
        retnum = int(nums[0])
    return retnum

# Roll 
# accept a string of ndm and roll that many dice. 
# also acepts @dregong roll 20 for legacy support
# returns one numeric value
def roll(diceStr):
    if 'd' in diceStr:
        nums = diceStr.split('d')
        count = int(nums[0])
        totalRoll = 0
        while count > 0:
            diceSize = int(nums[1])
            roll = random.randint(1,diceSize)
            totalRoll = totalRoll+roll
            count = count-1;
        return totalRoll
    else:
        num = int(diceStr.strip())
        return random.randint(1,num)

# reslultRoll
# accepts a ndm string, or n
# returns a string containing the comma separated roll results in square brackets [4, 6, 19, 13]
# and returns the sum of all the rolls done
# invokes roll as apropriate
def resultRoll(diceStr):
    nums = diceStr.split('d')
    if len(nums) == 1:
        # one number route
        t = roll(diceStr)
        retStr = "["+str(t)+"]"
        retNum = int(t)
        return retStr, retNum
    else:
        # two number path
        count = int(nums[0])
        retstr = "["
        total = 0
        while count > 0:
            t = roll("1d"+nums[1])
            retstr = retstr+str(t)+", "
            total = total+int(t)
            count = count-1
        retstr = retstr[:-2]+"]"
        return retstr, total
