# command handler function. 
# define all the command functions in here
# all command functions will return a content string that will be used
# to construct the reply message 
import random
import re
import globalVars
import dewgongUtils
from datetime import date
import datetime
import emoji
import discord
import threading
import time
import asyncio

# format roll: 
# @dewgong <roll> | <:game_die:> <number> <dnumber> <+number+number...> <help>
def rollCMD(textInput):
    # build a regex that matches 0 or 1 d followed by numbers and 0 or more +numbers for additives
    rollValRe = re.compile("^(\d*d?\d+) *(\+(\d)|(\d*d?\d+) *)*", re.IGNORECASE)
    textInput = textInput.lower()
    print("Is the string printable?")
    print(textInput.isprintable())
    inputArr = textInput.split('roll')

    # validate the roll expression found in inputArr[1]
    temp = inputArr[1].strip()
    resp = ""
    firstFlag = True
    maxNum = 0
    resultStr = ""
    resultNum = 0
    mathFlag = False
    # need a meme filter here. as long as text after the expr dosent break the regex we are fine
    temp2 = ""
    memeFilter = re.compile("(\d*d?\d+)",re.IGNORECASE)
    memeFilterMath = re.compile("(\+)",re.IGNORECASE)
    print("*********************************")
    print(temp)
    print(memeFilter.findall(temp))
    print(memeFilterMath.findall(temp))
    numbersArr = memeFilter.findall(temp)
    mathArr = memeFilterMath.findall(temp)

    for e in numbersArr:
        tresultNum = 0
        tresultStr = ""
        if ('d' in e) | firstFlag:
            tresultStr, tresultNum = dewgongUtils.resultRoll(e)
        else:
            tresultStr = e
            tresultNum = int(e.strip())
        firstFlag = False
        try:
            if mathArr.pop(0) == "+":
                resultStr = resultStr + tresultStr +" + "
                maxNum = maxNum + dewgongUtils.rollMax(e)
                resultNum = resultNum + tresultNum
                mathFlag = True
            else:
                resutStr = resultStr+tresultStr+ " - "
                maxNum = maxNum - dewgongUtils.rollMax(e)
                resultNum = resultNum - tresultNum
                mathFlag = True
        except IndexError:
            # We are done
            resultStr = resultStr + tresultStr
            maxNum = maxNum + dewgongUtils.rollMax(e)
            resultNum = resultNum + tresultNum
            print("Nothing to do here")
            break  #damn i wish i wasnt doing this
        print(tresultStr)
    print("*********************************")
    if (rollValRe.match(temp) or not mathFlag):
        # split around '+'     
        # if an element contains d|D process as a roll of that
        # if an element contains just a number, add that to the running sum
        # cover special case:
        # resultstr contains a breakdown of indivigual dice and mods
        # maxnum is the maximum potential roll
        # resultnum is the actual result
        # You rolled: <resultNum> out of <maxNum> (<resultStr>)
        # or
        # You rolled: <resultNum> emoji
        # this is broken in the case where there are several decimal digits in the string that are not part of the roll. 
    
        percent = int(3*(maxNum / 4))
        if resultNum == 1:
            emoji = globalVars.emojiList["ff"]
        elif resultNum > 1 and resultNum < (maxNum / 2):
            emoji = globalVars.emojiList["youtried"]
        elif resultNum >= (maxNum  / 2) and resultNum < percent:
            emoji = globalVars.emojiList["nepsmug"]
        elif resultNum >= percent and resultNum < maxNum:
            emoji = ":fire:"
        else:
            emoji = globalVars.emojiList["skeletor"]

        if not mathFlag:
            resp = "You rolled: " + str(resultNum) +" "+emoji
        else:
            resp = "You rolled: " + str(resultNum) +" out of " + str(maxNum) + " ("+resultStr+")"
    elif inputArr[1].lower() =="help" :
        resp = "Help Soon :tm:"
    else:
        resp = "Invalid Roll Command. Try Roll Help dingus."
    return resp
# end roll command

# format remind
# @dewgong remind (@user1) (@user2)... "reminder" <in x (hours) y (minutes) z (seconds)> |  <at nn:nn>  
def remindCMD(channel, textInput):
    # need to split the text input up into the required parts: 
    # the mentions, less dewgong, who will get sent messages
    # the reminder text which is what they will be sent
    # and the time to # end remind
    # send at

    parts = textInput.split("\"")
    # parts[0] = @dewgong remind @user1 @user2 ...
    mentions = parts[0].split("remind")[1]
    mentionArr = mentions.split()
    # parts[1] = To do a thing
    remtxt = parts[1]
    # parts[2] =at nn:nn | in aa:bb:cc
    temp = parts[2].split()
    timePart = temp[1].split(":")
    if temp[0] == "in":
        if not len(timePart) == 3:
            resp = "Specify Time as hh:mm:ss"
        else:
            hours = 0
            minutes = 0
            seconds = 0
            go = False
            try:
                hours = int(timePart[0])
                minutes = int(timePart[1])
                seconds = int(timePart[2])
                go = True
            except ValueError:
                resp = "Someone needs lo learn how to specify time. Use hh:mm:ss with only numbers next time."
            if go:
                for x in mentionArr:
                    seconds = seconds+(60*minutes)+(60*60*hours)
                    print(seconds)
                    task = asyncio.create_task(reminderSend(seconds, remtxt, channel, x))
                    #await reminderSend(seconds, remtxt, channel, x)
                resp = "Reminder set. Probably"

    return resp

async def reminderSend(sec, rem, chan, men):
    mtxt = men+" "+rem
    print("begin sleep")
    await asyncio.sleep(sec)
    await chan.send(content=mtxt)
    print("after await")

# end remind
# format 8ball
# @dewgong eightball | :8ball (meme text)
def eightballCMD(textInput):
    resp = ""
    respYes = [
                "It is inevitible.",
                "You bet your boots buckaroo.",
                "There exists a universe where it is not so. This is not one of them.",
                "This is the Way.",
                "Yes, and also buy a lotto ticket :wink:.",
                "Probably.",
                "Yeah, sure, whatever",
                "Holy Fuck yes",
                "The heart of the cards says: Yes.",
                "I want you to do it."
                ]
    respMaybe = [
                "I'm not going to tell you.",
                "I don't feel like checking.",
                "Ask again but nicer.",
                "You sure that was even a sentence?",
                "Its more fun if you don't know."
                ]
    respNo = [
                "Yikes. Oof. Yeah probably not.",
                "I looked at 14 million possible futures. In all of them, No.",
                "Yeah, thats a no from me dawg.",
                "No, also you really need to watch out for spiders today.",
                "No. *Dabs out on heelys*"
            ]
    # pick yes, no, maybe
    respType = random.choice(["yes","no","maybe"])
    if respType == "yes":
        resp = random.choice(respYes)
    elif respType == "no":
        resp = random.choice(respNo)
    else:
        resp = random.choice(respMaybe)
    
    return resp
#end 8ball

#format goagain
# @dewgong goagain
def goagainCMD(textInput):
    resp = ""
    wowClass = ""
    race = random.choice(["Human","Dwarf","Gnome","Night Elf","Orc","Undead","Tauren","Troll"])
    if race == "Human":
        wowClass = random.choice(["Warrior", "Paladin", "Rogue", "Priest", "Mage", "Warlock"])
    elif race == "Dwarf":
        wowClass = random.choice(["Warrior", "Paladin", "Hunter", "Rogue", "Priest"])
    elif race == "Gnome":
        wowClass = random.choice(["Warrior", "Rogue", "Mage", "Warlock"])
    elif race == "Night Elf":
        wowClass = random.choice(["Warrior", "Hunter", "Rogue", "Priest", "Druid"])
    elif race == "Orc":
        wowClass = random.choice(["Warrior", "Hunter", "Rogue", "Shaman", "Warlock"])
    elif race == "Undead":
        wowClass = random.choice(["Warrior", "Rogue", "Priest", "Mage", "Warlock"])
    elif race == "Tauren":
        wowClass = random.choice(["Warrior", "Hunter", "Shaman", "Druid"])
    elif race == "Troll":
        wowClass = random.choice(["Warrior", "Hunter", "Rogue", "Priest", "Shaman", "Mage"])
    part1 = random.choice(["This time let's try a ", "How about a ", "Maybe think about a ", "See if you can't fuck up a "])
    part2 = ", and "+random.choice(["don't forget to health potion this time.", "stay out of caves.", "maybe try not to die?", "I'll see you soon."])
    resp = part1+race+" "+wowClass+part2
    return resp;
#end goagain

# format burgertime
# @dewgong burgertime | :hungryburger:
def burgerCMD(textInput):
    resp = ""
    today = datetime.datetime.today().weekday()

    if str(today) == "4":
        #its friday, burger time
        now = datetime.datetime.now()
        midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        seconds = (now - midnight).seconds
        # burger time is at 12 noon. this is ecactly 12 hours from midnight 12*60*60 = 43200
        if seconds == 43200:
            resp = "Oh boy it is BURGER TIME! Run to Five Guys!"
        elif  seconds < 43200:
            timeleft = 43200 - seconds
            h = int(timeleft/3600)
            timeleft = timeleft-(h*3600)
            m = int(timeleft/60)
            timeleft = timeleft-(m*60)
            s = timeleft

            resp = "Get hyped! Burger time in: "+str(h)+" hours, "+str(m)+" minutes, "+str(s)+" seconds!"
        elif seconds > 43200 and seconds <= 46800:
            resp = "Red alert! Burger time is in progres! Get moving! Run!"
        else:
            resp = "Repent, Burger time has passed."

    else:
        if today == 3:
            resp = "Too soon! Burger time is tomorrow!"

        elif today > 4:
            resp = "Gone too soon, Burger time is a while from now."
        else:
            resp = "Too soon! Burger time isnt for "+str(4-today)+" more days!"
    return resp
# end burgertime

# format @dewgong shutdown
# only usable by Octobat
def shutdownCMD(textInput, requestor):
    resp = ""
    # schedule a shutdown
    if requestor == "Octobat#2864":
        resp = "Safe shutdown requested."
    else:
        resp = "You are not authorized to issue this command"

    return resp

