# Message handeler. 
# not using a class, depricates parser
# invoke functions from here when we need to deal with an incoming message
import globalVars 
import parser
import commands


def processMessage(msg, botUser):
    # process msg accepts a message and strips out any interesting data
    # calls functions based on what we found in the message
    # did we find a command?

    author = msg.author
    text = msg.content
    channel = msg.channel
    mentions = msg.mentions
    botMentioned = False
    botPrefix = False
    validCommand = False
    hasText = False
    msgRespStr = ""

    print(msg.type)
    if msg.author.bot == False:
        print(str(author) + " is not a bot")
        #pass the message off to the banking function to convert the message contents into a supply of boomercoin for the author
        # this may require the task infrastructure to be in place
        #implement the boomer hash function in dewgong utils
    else:
        print(str(author) + " is a bot")
    if botUser in mentions:
        botMentioned = True 
    else:
        botMentioned = False

    # check for the existance of attachemnts and non-text messages
    if len(text) == 0:
        print("No Text")
        hasText = False
    else:
        hasText = True

    if (hasText) and (text.split()[0] == globalVars.dewgongIDMobile) or (text.split()[0] == globalVars.dewgongIDPc):
        botPrefix = True
    else:
        botPrefix = False
    
    # we know if we are @ed and we are processing a command
    hasText = False
    if botMentioned and botPrefix:
        # ToDo: make this a boolean check that invokes a function to compare
        # the command key index (1) to all valid keywords including emojii
        com = parser.commandPicker(text)

        if (com == "roll"):
            tmpArr = text.split()
            tmpArr[1] = "roll"
            text = " ".join(tmpArr)
            msgRespStr = commands.rollCMD(text)
        
        elif (com == "burgertime"):
            tmpArr = text.split()
            tmpArr[1] = "burgertime"
            text = " ".join(tmpArr)
            msgRespStr = commands.burgerCMD(text)

        elif (com == "shutdown"):
            msgRespStr = commands.shutdownCMD(text, str(author))

        elif (com == "8ball"):
            tmpArr = text.split()
            tmpArr[1] = "8ball"
            text = " ".join(tmpArr)
            msgRespStr = commands.eightballCMD(text)

        elif (com =="remind"):
            msgRespStr = commands.remindCMD(channel, text)

        elif (com =="goagain"):
            msgRespStr = commands.goagainCMD(text)

        else:
            msgRespStr = "Use the Command Help Memelord"

    return msgRespStr
    
    

