# global variables that might be useful

#store all text and emoji inputs that can be used as command keywords, mapped to the command word they issue
commandList = {
        "roll" : "roll",
        "game_die" : "roll",
        "burgertime" : "burgertime",
        "hungryburger" : "burgertime",
        "shutdown" : "shutdown",
        "8ball" : "8ball",
        "pool_8_ball" : "8ball",
        "remind" : "remind",
        "goagain" : "goagain"
        }

dewgongIDMobile = "<@627230867932053504>"
dewgongIDPc = "<@!627230867932053504>"

# emojoi map, ones we care to embed in the 

emojiList = {
        "ff": "<:ff:619498385837391877>",
        "hungryburger": "<:hungryburger:627148280668618813>",
        "nepsmug" : "<:nepSmug:446442344363982859>",
        "skeletor": "<:skeletor:626416111809986560>",
        "youtried": "<:youtried:619504159930056736>",
        "youmustchoose": "<:youmustchoose:626417394977603603>"
        }
