# parser
# contains routines for handeling parsing things off the command text
import emoji
import globalVars

# given a string of input, decide if it represents a valid command
# and return which command it is
# or return Bad_Command if it is not structured properly
def commandPicker(msgContent):
    # command should always be at index 1 of the input string
    ttext = msgContent.split()[1]
    rettext = "heckin dank memes"
    # determine if ttext is an emoji here
    try:
        emotxt = (emoji.demojize(ttext)).split(":")[1]
        print("Emoji was:"+emotxt)
    except IndexError:
        emotxt = ttext

    if emotxt in globalVars.commandList:
        ttext = globalVars.commandList[emotxt]
    else:
        ttext = "not a command"

    print(ttext)
    # return to caller with a simplified result
    if (ttext == "roll"):
        print("returning roll")
        rettext = "roll"

    elif (ttext == "burgertime"):
        print("returning burgertime")
        rettext = "burgertime"

    elif (ttext == "shutdown"):
        print("returning shutdown")
        rettext = "shutdown"

    elif (ttext == "8ball"):
        print("returning 8ball")
        rettext = "8ball"

    elif (ttext == "remind"):
        print("returning remind")
        rettext = "remind"

    elif (ttext == "goagain"):
        print("returning go again")
        rettext = "goagain"

    else:
        print("returning invalid")
        rettext = "invalid"

    return rettext
    
# take in a string and return the appropriate emoji to plug directly into text
def emojiWrangler(request):
    # we can have standard and non standard emojies
    emostr = ""
    try:
        emostr = globalVars.emojiList[request]
    except KeyError:
        print("Not in custom emoji list")
    return emostr
